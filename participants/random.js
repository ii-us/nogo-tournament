const nogo = require('./../nogo.js');

/*
  A simple random player.
*/
module.exports = function () {
	return {
		init() {},
		startNextGame(color) {
			this.color = color;
			this.state = nogo.getInitialState();
		},
		/**
		 * @param array|null opponentMove In the form of: [x, y], null if it is the first move
		 * @returns array A move in the form of: [x, y]
		 */
		getNextMove() {
			const moves = nogo.getLegalMoves(this.state, this.color);
			const move = moves[Math.floor(Math.random() * moves.length)];
			this.state = nogo.move(this.state, this.color, [move.x + 1, move.y + 1]);
			return [move.x + 1, move.y + 1];
		},
		applyOpponentMove(opponentMove) {
			this.state = nogo.move(this.state, this.color === 'black' ? 'white' : 'black', opponentMove);
		},
		updateWithResult(winnerColor) {}
	}
};