# Turniej algorytmów sztucznej inteligencji dla gry NoGo

Niniejsze repozytorium pozwala rozegrać pojedynek(ki) pomiędzy algorytmami sztucznej inteligencji dla gry **NoGo**.

## Zasady gry NoGo

Przyjęte zostają reguły gry opisane na stronie [NoGo](https://webdocs.cs.ualberta.ca/~mmueller/nogo/rules.html). Przykładowe rozgrywki można znaleźć [tutaj](http://senseis.xmp.net/?AntiAtariGo) oraz [tutaj](http://combinatorialgametheory.blogspot.com/2010/11/game-description-nogo.html).

Dodatkowe założenia określają: rozmiar planszy wynosi **9x9**, maksymalny czas na wykonanie ruchu, to **5 sekund**, a grę rozpoczyna gracz **czarny**.

## Dodawanie swojego algorytmu

By utworzyć _gracza_ należy stworzyć plik w folderze `participants` zawierający 5 metod:
```javascript
/*
  Autorzy.
  Opis algorytmu.
*/
module.exports = function () {
	return {
		init() {},
		startNextGame(color) { /* ... */ },
		getNextMove() { /* ... */ },
		applyOpponentMove(opponentMove) { /* ... */ },
		updateWithResult(winnerColor) { /* ... */ }
	}
};
```
Metoda `init` wywoływana jest przed turniejem i służy do inicjacji _gracza_.

Metoda `startNextGame` wywoływana jest przez każdą grą i na jej wejście podawany jest kolor gracza `black` lub `white`.

Następnie w każdej turze gracz jest _proszony_ o ruch poprzez metodę `getNextMove`, która musi zwrócić tablicę zawierającą koordynaty ruchu w postaci `[x, y]`.

Koordynaty na mapie oznaczone są od 1 do 9 liczone od lewego górnego rogu, tj.
```
  | 1 2 3 4 5 6 7 8 9
--+------------------
1 | W B B B W W W B B
2 | W B B B B . W B W
3 | W W B W W B W B .
4 | . W B W B W B B B
5 | W W B W B . B W W
6 | W B B . B W W W W
7 | W B B W B W W B W
8 | W W B W B W W B W
9 | W W B B B B B B B
```

Po ruchu przeciwnika gracz otrzymuje informacje z koordynatami ruchu w postaci `[x, y]` poprzez metodę `applyOpponentMove`.

Po zakończonej grze _gracz_ otrzymuje informację o zwycięscy przez metodę `updateWithResult`, na wejście której podany jest kolor zwycięzcy.

Nazwa pliku jest automatycznie nazwą gracza, proszę o jej ograniczenie do 12 znaków.

## Turniej

Turniej rozgrywany jest w systemie każdy z każdym, pojedynczy mecz składa się z 100 gier (każdy program gra 50 jako czarny i 50 gier jako biały). Za wygraną w meczu gracz otrzymuje 1 punkt, za remis 0.5 oraz 0 w przypadku przegranej.

Turniej zostanie rozegrany na następującym sprzęcie:
- Intel Xeon W-2135 @ 3.70Ghz (6 rdzeni),
- 32 GB RAM,
- Ubuntu 20.04.1 LTS,
- wersja node.js-a podam w najbliższej przyszłości.

### Inne

Komenda `npm install` instaluje niezbędne biblioteki.

Komenda `npm run tournament` uruchamia zawody.

W repozytorium znajduje się implementacja losowego gracza, który jest dobrym punktem startu.
